import yup from "yup";

export const model_CentroCosto = yup.object({
    cc : yup.object({
        cc_nombre_proyecto : yup.string().required('El nombre es requerido'),
        cc_ubicacion:yup.string().required('La ubicacion de la tienda es requerido'),
        cc_cliente : yup.string().required("El nombre del cliente es requerido"),
        cc_direccion: yup.string().required("La direccion del proyecto es requerido"),
        cc_superficie:yup.string(),
        cc_unidad_negocio:yup.string().required('El area de negocio es requerido'),
        cc_tipo_negocio:yup.string().required('El tipo de negocio es requerido'),
        cc_jefe_proyecto:yup.string(),
        cc_jefe_area: yup.string(),
        cc_fecha_planimetria : yup.date(),
        cc_fecha_presupuesto: yup.date(),
        cc_fecha_inicio: yup.date(),
        cc_fecha_termino: yup.date(),
    })
})