import yup from "yup";

export const model_facturar = yup.object({

    fact: yup.object({
        detalle: yup.string().required('El detalle es requerido'),
        monto: yup.string().required('El monto , de la solicitud de factura  es requerido'),
        oc: yup.string(),
        num_factura: yup.string().required('El numero de la factura es requerido'),
    })

})