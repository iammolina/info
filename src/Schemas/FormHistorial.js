import yup from "yup";

export const FormHistorial = yup.object({
    cc: yup.object({
        cc_observacion: yup.string()
                            .required('No puedes ingresar un comentario vacio')
                            .min(20 , 'El comentario debe tener al menos 20 caracteres')


    })
})