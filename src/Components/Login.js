import React, { Component } from 'react';
import Form from 'react-formal';
import { Redirect } from 'react-router';
import { modelSchema } from '../Schemas/Login';
import { Auth } from '../Api/usuarios';

import '../App.css';

export default class Login extends Component {

    constructor(){
        super();
        this.state = {
            model: modelSchema,
            errorLogin: false,
            redirect : false

        }
    }

    onSubmit(formValues) {
       
        Auth(formValues.user)
            .then((response) => 
            {
                if (response.data.msg === "success") 
                {
                    localStorage.setItem('user', JSON.stringify(response.data.data));
                    this.setState({ redirect: true });
                }

            })
            .catch((error) => {
                this.setState({
                    errorLogin: true
                })
            })

    }

    render() 
    {
        const { errorLogin, model , redirect } = this.state;

        if (redirect) {
            return <Redirect to='/' />;
        }

        return (
            

            <div className="container">

             <div className="login">

                        <img className="App-logo img-responsive" src="/logo.png" align="middle" alt="logo" />

                        <h2 className="text-center"> info </h2>


                        {errorLogin === true &&
                            <div className="alert alert-danger">
                                Datos de login incorrectos
                                </div>
                        }


                        <Form
                            onSubmit={(e) => this.onSubmit(e)}
                            schema={modelSchema}
                            value={model}
                            onChange={modelInfo => this.setState({ model: modelInfo })}
                        >

                            <div style={{ "marginTop": "10px" }} className="form-group">
                                <label>Email</label>
                            <Form.Field className={`form-control col-md-12`} name='user.username' placeholder='email@instore.cl' />
                            <Form.Message errorClass={`col-md-12 alert alert-danger`} for='user.username' />
                            </div>

                            <br />

                            <div className="form-group">
                                <label>Clave</label>
                            <Form.Field type="password" className={`form-control col-md-12`} name='user.password' placeholder='***********' />
                                <Form.Message errorClass={`col-md-12 alert alert-danger`} for='user.password' />
                            </div>

                            <br />

                            <Form.Button style={{ "marginTop": "10px" }} className={`btn btn-block btn-info`} type='submit'>
                                Login
                            </Form.Button>
                        </Form>
                </div>


            </div>
        )
    }
}