import React from 'react';

const EmpresaMenuItem = ({ emp }) => (
    <div>
        <span><strong>{emp.name}</strong></span>
    </div>
);

export default EmpresaMenuItem;