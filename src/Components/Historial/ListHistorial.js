import React from "react";
import Form from 'react-formal';
import jwtDecode from 'jwt-decode';
import HistorialCc from './HistorialCc';
import { FormHistorial } from '../../Schemas/FormHistorial';
import { getHistorial, postHistorial } from '../../Api/centrocosto';
import 'moment-timezone';


export default class ListHistorial extends React.Component {
    
    constructor() {
        super();

        this.state = {
            model: FormHistorial,
            cc: {},
            comentarios: [],
            user: JSON.parse(localStorage.getItem('user'))
        }

        this._AddHistorial = this._AddHistorial.bind(this);

    }


    componentWillReceiveProps()
    {
        const { id } = this.props;

        getHistorial(id)
            .then((response) => {

                this.setState({
                    comentarios: response.data
                })

                this.forceUpdate()

            }).catch((error) => {
                alert('error');
            });

       

    }

    componentDidMount() {

        const { id } = this.props;


        getHistorial(id)
            .then((response) => {

                this.setState({
                    comentarios: response.data
                })

                this.forceUpdate()

            }).catch((error) => {
                alert('error');
            });

    }

    _AddHistorial(e) 
    {
        const { id } = this.props;

        const u = jwtDecode(this.state.user);

        console.log(this.myFormRef);

        var hist = {
            id_usuario: u[0].id_usuario ,
            id_centro_costo: id,
            observacion: e.cc.cc_observacion,
            create_at: (Date.now() / 1000)
        };
        
        postHistorial(id,hist)
            .then((response) => {
                this.componentDidMount();
                
                this.forceUpdate();
            }).catch((error) => {
                console.log(error);
            });

    }

    render() {

        const { comentarios, model } = this.state;

        return (
            <div>

                <br />

                <Form
                    onSubmit={(e) => this._AddHistorial(e)}
                    schema={FormHistorial}
                    value={model}
                    onChange={modelInfo => this.setState({ model: modelInfo })}
                >
                    <div className="clearfix"></div>


                    <div style={{ "marginTop": "10px" }} className="form-group">

                        <Form.Field type="textarea" className={`form-control col-md-12`} name='cc.cc_observacion' placeholder='Escriba un comentario ...' />
                        <Form.Message errorClass={`col-md-12 alert alert-danger`} for='cc.cc_observacion' />

                    </div>

                    <div className="clearfix"></div>

                    <Form.Button style={{ "marginTop": "10px" }} className={`btn btn-block btn-success`} type='submit'>Guardar</Form.Button>

                </Form>

                <br />


                <HistorialCc historial={comentarios} />


            </div>
        );
    }




}
