import React from "react";
import { Table } from "react-bootstrap";
import { LinkContainer } from 'react-router-bootstrap';
import Moment from 'react-moment';
import 'moment-timezone';


class ListTable extends React.Component {

    render() {

        const { proyectos } = this.props;
        
        return (
            <tr>
                <td>
                    <LinkContainer to={`/proyecto/${proyectos.id_proyecto}`}>
                        <a>{proyectos.id_proyecto}</a>
                    </LinkContainer>
                </td>
                <td>{proyectos.cc_nombre_proyecto}</td>
                <td> <Moment unix format="YYYY/MM/DD">{proyectos.create_at}</Moment></td>
            </tr>

        );
    }
}

export default class ProyectosInactivos extends React.Component {

    render() {

        const { proyectos } = this.props;


        if (proyectos.length === 0) {

            return (
                <div>No existen proyectos</div>
            )

        }


        const createProyecto = (pro, index) => {
            return <ListTable key={index} proyectos={pro} />

        }

        return (



            <Table striped bordered>
                <thead>
                    <tr>
                        <th className="text-center">CC</th>
                        <th>Nombre Proyecto</th>
                        <th>Fecha Creacion</th>
                    </tr>
                </thead>
                <tbody>

                    {proyectos.map(createProyecto)}

                </tbody>
            </Table>




        )

    }



}
