import React from "react";
import Moment from 'react-moment';
import ListHistorial from '../Historial/ListHistorial';
import { PutCc } from "../../Api/centrocosto";
import { LinkContainer } from 'react-router-bootstrap';
import { Tabs, Tab, Table, Button, Glyphicon, Modal } from "react-bootstrap";
import 'moment-timezone';

class ListSelect extends React.Component 
{
    constructor(props) {
        super(props);
        this.state = {
            values: [
                { name: '1', id: 1 },
                { name: '2', id: 2 },
                { name: '3', id: 3 },
                { name: '4', id: 4 },
                { name: '5', id: 5 },
                { name: '6', id: 6 },
                { name: '7', id: 7 }
            ]
        };
    }

    render() {

        const { id } = this.props;
        
        return (
           
            this.state.values.map(v => (

                <option value={v.id} selected={id === v.id} key={v.id} >{v.name}</option>
               
            ))
    
        );

        
    }
}

class ListTable extends React.Component {

    constructor(props)
    {
        super(props);
        this.state = {
             color : null
        };

        this.handleChange = this.handleChange.bind(this);
    }  

    handleChange(i , event)
    {
         const id = parseInt(event.target.value, 10);

         const data ={ 'fase' : id };

          PutCc(i , data)
            .then((response) => 
            {
                
                if (id === 1) this.setState({ color: 'default' });
                if (id === 2) this.setState({ color: 'fase-dos' });
                if (id === 3) this.setState({ color: 'fase-tres' });
                if (id === 4) this.setState({ color: 'fase-cuatro' });
                if (id === 5) this.setState({ color: 'fase-cinco' });
                if (id === 6) this.setState({ color: 'cambios' });
                if (id === 7) this.setState({ color: 'pendientes' });

            }).catch((error) => 
            {
                alert('error');
            });
         
    }

    
    
    render() {

        const { proyectos, onModal } = this.props;

        return (
            <tr>
                <td className="default">
                    <LinkContainer to={`/proyecto/${proyectos.id_proyecto}`}>
                        <a>{proyectos.centro_costo}</a>
                    </LinkContainer>
                </td>

                {proyectos.fase === 1 &&
                    <td className={this.state.color || "default"}>

                        <select onChange={(e) => { this.handleChange(proyectos.id_proyecto, e) }}>

                            <ListSelect id={proyectos.fase} />

                        </select>

                    </td>
                }


                {proyectos.fase === 2 &&

                    <td className={this.state.color || "fase-dos"}>

                        <select onChange={(e) => { this.handleChange(proyectos.id_proyecto, e) }}>

                            <ListSelect id={proyectos.fase} />

                        </select>

                    </td>
                }

                {proyectos.fase === 3 &&
                    <td className={this.state.color || "fase-tres"}>

                        <select onChange={(e) => { this.handleChange(proyectos.id_proyecto, e) }}>

                            <ListSelect id={proyectos.fase} />

                        </select>

                    </td>
                }


                {proyectos.fase === 4 &&
                    <td className={this.state.color || "fase-cuatro"}>

                        <select onChange={(e) => { this.handleChange(proyectos.id_proyecto, e) }}>

                            <ListSelect id={proyectos.fase} />

                        </select>

                    </td>
                }

                {proyectos.fase === 5 &&
                    <td className={this.state.color || "fase-cinco"}>

                        <select onChange={(e) => { this.handleChange(proyectos.id_proyecto, e) }}>
                            <ListSelect id={proyectos.fase} />
                        </select>

                    </td>
                }


                {proyectos.fase === 6 &&
                    <td className={this.state.color || "cambios"}>
                        <select onChange={(e) => { this.handleChange(proyectos.id_proyecto, e) }}>
                            <ListSelect id={proyectos.fase} />
                        </select>
                    </td>
                }


                {proyectos.fase === 7 &&
                    <td className={this.state.color || "pendientes"}>

                        <select onChange={(e) => { this.handleChange(proyectos.id_proyecto, e) }}>

                            <ListSelect id={proyectos.fase} />

                        </select>

                    </td>
                }
               
                <td>{proyectos.cc_nombre_proyecto}</td>
                <td>{proyectos.nombre_jefe_proyecto}</td>
                <td>{proyectos.area}</td>
                <td>{proyectos.cc_ubicacion}</td>
                <td>{proyectos.cc_cliente}</td>
                <td><Moment unix format="YYYY/MM/DD">{proyectos.cc_fecha_planimetria}</Moment></td>
                <td>{proyectos.responsable}</td>
                <td><Moment unix format="YYYY/MM/DD">{proyectos.cc_fecha_presupuesto}</Moment></td>
                <td><Button bsStyle="info" bsSize="xsmall" onClick={onModal} ><Glyphicon glyph="glyphicon glyphicon-edit" /></Button></td>

            </tr>
           
        );
    }
}

export default class ProyectosHome extends React.Component
{
    constructor(props, context) 
    {
        
        super(props, context);
        this.handleShow = this.handleShow.bind(this);
        this.handleClose = this.handleClose.bind(this);
        this._update_proyecto = this._update_proyecto.bind(this);

        this.state = {
            show: false,
            id : 0
        };

    }

    handleClose() {
        this.setState({ show: false });
    }

    handleShow(i) {
        this.setState({ show: true , id : i });
    }

    _update_proyecto(e) 
    {
        var pyt = {};
        
        if(e.target.name === 'estado')
        {
             pyt = { estado: e.target.value };
        }

        if (e.target.name === 'responsable') {
             pyt = { responsable : e.target.value };
        }
       
        PutCc(this.state.id, pyt)
            .then((response) => 
            {
                

            }).catch((error) => {
                alert('error');
            });
    }

    

    render()
    {
        const { proyectos } = this.props;

        if (proyectos.length === 0) {

            return (
                
                <div className="text-center">

                    <Glyphicon glyph="glyphicon glyphicon-search"  />

                    <h1>Buscar</h1>
                    <h2><small>No se encontraron proyectos</small></h2>
                    
                </div>
            )

        }


        const createProyecto = (pro , index) => 
        {
            return <ListTable key={index} proyectos={pro} onModal={() => this.handleShow(pro.id_proyecto) } />
        }

        return(


            <Table striped bordered>
                <thead>

                    <tr>
                        <td>Creacion 1</td>
                        <td className="fase-dos">En Arquitectura 02</td>
                        <td className="fase-tres">Cotizacion y Presupuesto 03</td>
                        <td className="fase-cuatro">En aprobacion 04</td>
                        <td className="fase-cinco">Proyecto Activo 05 </td>
                        <td className="cambios">Proyecto con Cambios 06 </td>
                        <td className="pendientes">Proyecto con Pendientes 07 </td>
                    </tr>

                    <tr>
                        <th className="text-center">CC</th>
                        <th>Fase</th>
                        <th>Nombre Proyecto</th>
                        <th>Jefe Proyecto</th>
                        <th>Area Proyecto</th>
                        <th>Ubicacion</th>
                        <th>Project Manager</th>
                        <th>Fecha Cotizacion</th>
                        <th>Resp Presupuesto</th>
                        <th>Fecha Presupuesto</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                   
                    {proyectos.map(createProyecto)} 

                </tbody>

                
                <Modal show={this.state.show} onHide={this.handleClose}>
                    <Modal.Header closeButton>
                        <Modal.Title>Historial</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                       
                        <Tabs defaultActiveKey={2} id="uncontrolled-tab-example">
                            <Tab eventKey={1} title="Datos">
                                
                                <div className="form-group">

                                    <label>Estado</label>

                                    <select className="form-control" name="estado" onChange={this._update_proyecto} >
                                        <option value="3">Stand By</option>
                                        <option value="4">No se Construye</option>
                                        <option value="2">Terminado</option>
                                    </select>

                                </div>

                                <div className="form-group">

                                    <label>Responsable:</label>
                                    <select className="form-control" name="responsable" onChange={this._update_proyecto}>
                                        <option value="139">Jenny Kraudy </option>
                                        <option value="136">Maria Caro </option>
                                        <option value="137">Francisca Schleyer </option>
                                        <option value="138">Rodrigo Herbias </option>
                                        <option value="140">Jose Salcedo </option>        
                                    </select>
                                
                                </div>

                            </Tab>
                            <Tab eventKey={2} title="Historial">

                                <ListHistorial id={this.state.id} />

                            </Tab>
                            
                        </Tabs>

                    </Modal.Body>
                    <Modal.Footer>
                        <Button onClick={this.handleClose}>Cerrar</Button>
                    </Modal.Footer>
                </Modal>



            </Table>


            
           
        )

    }



}
