import React, { Component } from 'react';
import BarraNavegacion from '../BarraNavegacion';
import ListHistorial from '../Historial/ListHistorial';
import UpdateCentroCosto from '../../Forms/UpdateCentroCosto';
import ListOcSoftland from '../Proyectos/Tabs/Listar_Oc_softland';
import ListNotasVentas  from '../Proyectos/Tabs/Listar_Notas_Ventas';
import ListOcIconstruye from '../Proyectos/Tabs/Listar_Oc_Iconstruye';
import ListSubcontratos from '../Proyectos/Tabs/Listar_Subcontratos';
import { getId, PutCc, getConsultas } from '../../Api/centrocosto';
import { Tab, Tabs, Media } from 'react-bootstrap';


class ProyectoId extends Component {

    constructor()
    {
        super();

        this.state =
            {
             cc: null,
             isLoading: false,
             data: [],
             totalnv: 0 
            }
       
        this._update_proyecto = this._update_proyecto.bind(this);
        this.handleSelect = this.handleSelect.bind(this);
        this.consultas = this.consultas.bind(this);
       
       
    }

    componentDidMount()
    {
        const { match } = this.props;

        
        getId(match.params.id)        
            .then((response) => {

                this.setState({ cc : response.data[0] });


            }).catch((error) => {
                alert('error');
            });
    }


    _update_proyecto(id) 
    {
        var pyt = { activo : id }; 

        const { match } = this.props;

        this.setState({ isLoading: true });

        PutCc(match.params.id, pyt)
            .then((response) => {

                this.setState({ isLoading: false });

                this.componentDidMount();

                this.forceUpdate();

            }).catch((error) => {
                alert('error');
            });
    }


    consultas(url)
    {

        getConsultas(url)
            .then((response) => 
            {

                this.setState({ data: response.data.datos, totalnv: response.data.total[0] });
                this.forceUpdate();

            }).catch((error) => {
                alert('error');
            });
       
    }


    handleSelect(key)
    {
        if (key === 2) {
            const url = 'centrocosto/softland/ociconstruye/' + this.state.cc.centro_costo;
            this.consultas(url);
        }

        if (key === 3) {
            const url = 'centrocosto/softland/subcontratos/' + this.state.cc.centro_costo;
            this.consultas(url);
        }


        if (key === 4) 
        {
            const url = 'centrocosto/softland/nv/' + this.state.cc.centro_costo;
            this.consultas(url);
        }


        if (key === 7) {
            const url = 'centrocosto/softland/oc/' + this.state.cc.centro_costo;
            this.consultas(url);
        }

    }
 

    render()
    {

        const { match } = this.props;
        const { cc } = this.state;

        if (!cc) return null;

       
        cc.cc_fecha_inicio = new Date(cc.cc_fecha_inicio * 1000);
        cc.cc_fecha_termino = new Date(cc.cc_fecha_termino * 1000);
        cc.cc_fecha_presupuesto = new Date(cc.cc_fecha_presupuesto * 1000);
        cc.cc_fecha_planimetria = new Date(cc.cc_fecha_planimetria * 1000);

        return (
            <div>
                <BarraNavegacion />
                
                <div className="container">

                    <Media>
                        <Media.Left>
                            <img width={64} height={64} src="/image.png" alt="thumbnail" />
                        </Media.Left>
                        <Media.Body>
                          
                            <blockquote>
                                <h2>{cc.centro_costo} - {cc.cc_nombre_proyecto}</h2>
                                <footer> <a>{cc.cc_cliente}</a> <cite title="Source Title"> { cc.cc_ubicacion }</cite></footer>

                                {cc.estado === 1 &&

                                    <footer> Estado:<strong> Status de Proyecto</strong> </footer>
                                }

                                
                                {cc.estado === 2 &&
                                
                                    <footer> Estado:<strong> Stand By</strong> </footer>
                                }

                                {cc.estado === 3 &&

                                    <footer> Estado:<strong> No se contruye</strong> </footer>
                                }

                                {cc.estado === 4 &&

                                    <footer> Estado:<strong> Terminados</strong> </footer>
                                }


                            </blockquote>
{/*                            

                                {cc.activo === 0 &&
                                    <ButtonToolbar>
                                        <Button
                                            bsStyle="success"
                                            disabled={this.state.isLoading}
                                            onClick={!this.state.isLoading ? (e) => this._update_proyecto(1) : null}
                                        >
                                            {this.state.isLoading ? 'Espere un momento...' : 'Activar'}
                                        </Button>

                                        <Button onClick={(e) => this._update_proyecto(2)} bsStyle="default">Archivar</Button>
                                    </ButtonToolbar>
                               }


                                {cc.activo === 1 &&
                                    <ButtonToolbar>
                                        <Button
                                            bsStyle="danger"
                                            disabled={this.state.isLoading}
                                            onClick={!this.state.isLoading ? (e) => this._update_proyecto(3) : null}
                                        >
                                            {this.state.isLoading ? 'Espere un momento...' : 'Terminar'}

                                        </Button>

                                        <Button onClick={(e) => this._update_proyecto(2)} bsStyle="default">Archivar</Button>
                                    </ButtonToolbar>
                               }


                            {cc.activo === 2 &&
                                <ButtonToolbar>
                                    <Button onClick={(e) => this._update_proyecto(1)} bsStyle="default">Desarchivar</Button>
                                </ButtonToolbar>
                            }
                   */}

                        </Media.Body>
                    </Media>
                    <br/>

                    </div>

                    <div className="container-fluid">
                    <Tabs defaultActiveKey={1} id="uncontrolled-tab-example" onSelect={this.handleSelect}>

                            <Tab eventKey={1} title="HISTORIAL">

                                 <div className="container">

                                     <ListHistorial id={match.params.id} />

                                 </div>

                            </Tab>
                      

                        <Tab eventKey={2} title="OC ICONSTRUYE">
                            

                            <ListOcIconstruye ocicons={this.state.data} totalnv={this.state.totalnv} />


                        </Tab>

                        <Tab eventKey={3} title="SUB-CONTRATOS">



                            
                            <ListSubcontratos subcontratos={this.state.data} totalnv={this.state.totalnv} />

                           
                        </Tab>

                        <Tab eventKey={4} title="NOTAS DE VENTAS">

                            <ListNotasVentas notasdeventa={this.state.data} totalnv={this.state.totalnv} />
                           
                        </Tab>

                        {/* <Tab eventKey={5} title="FACTURAS COMPRAS">
                           
                        </Tab>
                        <Tab eventKey={6} title="FACTURAS VENTAS">
                           
                        </Tab> */}

                        <Tab eventKey={7} title="OC SOFTLAND">
                                

                            <ListOcSoftland ocsoftl={this.state.data} totalnv={this.state.totalnv} />


                        </Tab>


                        <Tab eventKey={8} title="INFORMACION">

                            <div className="container">
                                <UpdateCentroCosto cc={cc} />  

                                <div className="espacio">
                                </div> 
                            </div>
                        </Tab>
                       
                    </Tabs>
                    
                </div>
            </div>
        );
    }
}

export default ProyectoId;