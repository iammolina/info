import React from "react";
import Moment from 'react-moment';
import UpdateValidarCentroCosto from '../../Forms/UpdateValidarCentroCosto';
import EnvioCentroCosto from '../Envios/EnvioCentrocosto';
import { getId, activarCC, addHarvest, emailconfirmacion, addSoftland} from "../../Api/centrocosto";
import { LinkContainer } from 'react-router-bootstrap';
import { Table, Button, Glyphicon, Modal, FormGroup, ControlLabel, FormControl } from "react-bootstrap";
import 'moment-timezone';

class ListTable extends React.Component {

    render() 
    {
        const { proyectos, onModal, onModal_aceptar } = this.props;
        return (
            <tr>
                <td>
                    <LinkContainer to={`/proyecto/${proyectos.id_proyecto}`}>
                        <a>{proyectos.id_proyecto}</a>
                    </LinkContainer>
                </td>
                <td>{proyectos.cc_nombre_proyecto}</td>
                <td>{proyectos.area}</td>
                <td>{proyectos.cc_ubicacion}</td>
                <td> <Moment unix format="YYYY/MM/DD">{proyectos.create_at}</Moment></td>

                <td><Button bsStyle="info" bsSize="xsmall" onClick={onModal} ><Glyphicon glyph="glyphicon glyphicon-edit" /></Button></td>

                <td><Button bsStyle="success" bsSize="xsmall" onClick={onModal_aceptar}  ><Glyphicon glyph="glyphicon glyphicon-ok" /></Button></td>
            </tr>
        );
    }
}

export default class ProyectosValidar extends React.Component {

    constructor(props) {

        super(props);
       
        this.state = {
            centrocosto:[],
            show: false,
            show_aceptar: false, 
            show_aceptado : true,
            btn_disabled: false,
            estado_harvest : '',
            estado_softland: '',
            estado_mail: '',
            text_estado_harvest: '',
            text_estado_softland: '',
            text_estado_mail: '',
            id: 0
        };

        this.handleShow = this.handleShow.bind(this);
        this.handleClose = this.handleClose.bind(this);
        this.handleShow2 = this.handleShow2.bind(this);
        this._UpdateCc = this._UpdateCc.bind(this);

    }


    handleClose() {

        this.props.select(2);

        this.setState({
                show: false ,
                show_aceptar: false,
                show_aceptado: true ,
                show_mail: false,
                show_aceptado_continuar : false,
                show_aceptado_continuar_btn: false,
                btn_disabled : false,
                estado_harvest: '',
                estado_softland: '',
                estado_mail: '',
                text_estado_harvest: '',
                text_estado_softland: '',
                text_estado_mail: '',
                });
    }



    _UpdateCc(id)
    {   
      
        activarCC(id).then((response) => 
        {
            this.setState({ show_aceptado: false, show_aceptado_continuar: true });

            var data = this.state.centrocosto;

            addHarvest(data).then((response) => 
            {
                this.setState({ estado_harvest: 'success' , text_estado_harvest : 'Proyecto Creado' });

                this._softland(id);

            }).catch((error) => 
            {
                alert('error');
            });


        }).catch((error)=>
        {
            alert(error);
        })


    }

    _softland(id) 
    {
        addSoftland(id).then((response) => 
        {
            this.setState({ estado_softland: 'success' , text_estado_softland : 'Proyecto creado' });
            this._enviomail();


        }).catch((error) => 
        {

            this.setState({ estado_softland: 'error', text_estado_mail: 'Proyecto no creado , ' + error });
            this._enviomail();

        });
    }


    _enviomail()
    {
        var data = this.state.centrocosto;

        emailconfirmacion(data).then((response) => 
        {
            this.setState({ estado_mail: 'success', text_estado_mail: 'Email enviado', show_aceptado_continuar_btn : true });
        
        }).catch((error) => 
        {

            this.setState({ estado_mail: 'error', text_estado_mail: 'Email no enviado , ' + error, show_aceptado_continuar_btn: true });

        });
    }


    _continuar()
    {
        this.setState({ show_mail: true , show_aceptado: false , show_aceptado_continuar : false });
    }

    
    handleShow(i) 
    {
        getId(i)
            .then((response) => 
            {
                this.setState({ centrocosto : response.data[0] ,  show: true });

            }).catch((error) => {

                console.log(error);

                alert('error');
            });
    }

    handleShow2(i) {
        getId(i)
            .then((response) => {
          

                this.setState({ centrocosto: response.data[0], show_aceptar: true });

            }).catch((error) => {

                console.log(error);

                alert('error');
            });
    }

    render() {

        const { proyectos } = this.props;

        if (proyectos.length === 0) {

            return (
                <div>No existen proyectos</div>
            )

        }

        const createProyecto = (pro, index) => {
            return <ListTable key={index} proyectos={pro} onModal={() => this.handleShow(pro.id_proyecto)} onModal_aceptar={() => this.handleShow2(pro.id_proyecto)} />
        }

        return (

            <Table striped bordered>
                <thead>
                    <tr>
                        <th class="text-center">ID</th>
                        <th>Nombre Proyecto</th>
                        <th>Area</th>
                        <th>Ubicacion</th>
                        <th>Fecha Creacion</th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    {proyectos.map(createProyecto)}
                </tbody>


                <Modal show={this.state.show} onHide={this.handleClose}>
                    <Modal.Header closeButton>
                        <Modal.Title>Centro Costo #ID:{this.state.centrocosto.id_proyecto}</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>

                        <UpdateValidarCentroCosto cc={this.state.centrocosto}/>

                    </Modal.Body>
                    <Modal.Footer>
                        <Button onClick={this.handleClose}>Cerrar</Button>
                    </Modal.Footer>
                </Modal>

                <Modal show={this.state.show_aceptar} onHide={this.handleClose}>
                    <Modal.Header closeButton>
                        <Modal.Title>Centro de costo #ID:{this.state.centrocosto.id_proyecto}</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>

                        {this.state.show_aceptado &&

                            <div>

                                <h1 class="text-center">Aceptas el Centro Costo </h1>

                                <h1 class="text-center"><small>{this.state.centrocosto.cc_nombre_proyecto}</small></h1>

                            </div>

                        }

                        {this.state.show_aceptado_continuar  &&
                            <form>

                                <FormGroup controlId="formValidationSuccess1" validationState={this.state.estado_harvest}>
                                    <ControlLabel>Harvest</ControlLabel>
                                    <FormControl type="text" value={this.state.text_estado_harvest} disabled/>
                                    <FormControl.Feedback />
                                </FormGroup>

                                <FormGroup controlId="formValidationSuccess1" validationState={this.state.estado_softland}>
                                    <ControlLabel>Softland</ControlLabel>
                                    <FormControl type="text" value={this.state.text_estado_softland} disabled />
                                     <FormControl.Feedback />
                                </FormGroup>

                                <FormGroup controlId="formValidationSuccess1" validationState={this.state.estado_mail}>
                                    <ControlLabel>Email de confirmacion</ControlLabel>
                                    <FormControl type="text"   value={this.state.text_estado_mail} disabled  />
                                    <FormControl.Feedback />
                                </FormGroup>
                            </form>
                       }
                        
                        {this.state.show_mail &&

                             <EnvioCentroCosto id={this.state.centrocosto.id_proyecto}/> 
                        }

                    </Modal.Body>
                    <Modal.Footer>

                        {this.state.show_aceptado &&

                        <div>
                            <Button bsStyle="success" onClick={() => this._UpdateCc(this.state.centrocosto.id_proyecto)} >Si</Button>
                            <Button bsStyle="danger" onClick={this.handleClose}>No</Button>
                        </div>

                        }

                        {this.state.show_aceptado_continuar_btn &&

                            <div>
                                <Button bsStyle="success" onClick={() => this._continuar()} >Continuar</Button>
                                <Button bsStyle="danger" onClick={this.handleClose}>No</Button>
                            </div>

                        }

                    </Modal.Footer>
                </Modal>

            </Table>

        )

    }



}
