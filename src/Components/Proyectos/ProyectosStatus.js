import React, { Component } from 'react';
import { Tabs, Tab} from "react-bootstrap";
import ProyectosActivos from './ProyectosActivos';
import ProyectosInactivos from './ProyectosInactivos';
import { AsyncTypeahead } from 'react-bootstrap-typeahead';
import { getInactivos, getFiltrado, findUsers } from '../../Api/centrocosto';
import UserMenuItem from "../UserMenuItem";



class ProyectosStatus extends Component {

    constructor(props,context)
    {
        super(props, context);
        this.state = 
        {
          key: 1,
          activosProyectos: [],
          inactivosProyectos:[],
          jefe_proyecto: [],
          value:0,
          item_select:0,
          item_value : '',
          isLoading : false
        }

        this.handleSelect = this.handleSelect.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange2 = this.handleChange2.bind(this);
    }

    handleChange2(event) 
    {
        if(event.target.name === 'item')
        {
            this.setState({ item_select : event.target.value });
        }

        if (event.target.name === 'item_value') {
            this.setState({ item_value: event.target.value });
        }

    }

    componentDidMount()
    {
        const _perArray = [];
        const data = [{ 'personas': _perArray, 'area': this.state.value, 'item': { 'select': this.state.item_select, 'value': this.state.item_value } }];
        getFiltrado(data).then((response) => 
        {
            this.setState({
                activosProyectos: response.data
            })
        })
        .catch((error) => {
                alert('error');
        });
    }


    handleChange(event) 
    {
        this.setState({ value: event.target.value });
    }

    handleSubmit(event) 
    {

        const _perArray = [];

        for (let index = 0; index < this.state.jefe_proyecto.length; index++) 
        {
            _perArray.push(this.state.jefe_proyecto[index].id_usuario);
        }
        
        const data = [{ 'personas': _perArray , 'area' : this.state.value , 'item' : { 'select' : this.state.item_select , 'value' : this.state.item_value } }];


        getFiltrado(data).then((response) => 
        {
            this.setState({
                activosProyectos: response.data
            })
        })
        .catch((error) => {

            alert('error');

        });
        
    }


    _handleSearch = (query) => {
        this.setState({ isLoading: true });
        var datos = { data: query };
        findUsers(datos) 
            .then((response) => {
                this.setState({
                    isLoading: false,
                    options: response.data.items
                });

            })
    }



    handleSelect(key)
    {
        getInactivos(key).then((response) => 
            {
                this.setState({
                    inactivosProyectos: response.data
                })
            })
            .catch((error) => {

                alert('error');

            });
    }


    render() {
        return (

             <div>

                <div>

                     <Tabs defaultActiveKey={this.state.key} onSelect={this.handleSelect}  id="uncontrolled-tab-example">

                        <Tab eventKey={1} title="Status">
                            <br/>


                            <div className="row">


                                <div className="form-group col-md-2">
                                    <select className="form-control" name="item" value={this.state.item_select} onChange={this.handleChange2} >
                                        <option value={0}>TODOS</option>
                                        <option value={'centro_costo'}>CC</option>
                                        <option value={'cc_nombre_proyecto'}>Nombre Proyecto</option>
                                        <option value={'cc_ubicacion'}>Ubicacion</option>
                                        <option value={'cc_cliente'}>Project Manager</option>
                                    </select>
                                </div>

                                <div className="form-group col-md-3">
                                    
                                    <input className="form-control" name="item_value" placeholder="Buscar" value={this.state.item_value} onChange={this.handleChange2}/>

                                </div>
                               

                                <div className="form-group col-md-3">

                                    <select className="form-control" name="estado" value={this.state.value} onChange={this.handleChange} >
                                        <option value={0}>Seleccionar área o unidad de negocio...</option>
                                        <option value={1}>ADMINISTRACIÓN INSTORE</option>
                                        <option value={2}>CORNER</option>
                                        <option value={4}>STAND ALONE</option>
                                        <option value={5}>GRANDES TIENDAS</option>
                                        <option value={6}>OTROS PROYECTOS</option>
                                        <option value={7}>COLOMBIA</option>
                                    </select>

                                </div>


                                <div className="col-md-3">
                                        <AsyncTypeahead
                                            {...this.state}
                                            labelKey="nombre_usuario"
                                            minLength={3}
                                            multiple
                                            onSearch={this._handleSearch}
                                            onChange={(selected) => {
                                                this.setState({ jefe_proyecto : selected });
                                            }}
                                            placeholder="Buscar el jefe de proyecto ..."
                                            renderMenuItemChildren={(option) => (
                                                <UserMenuItem key={option.id} user={option} />

                                            )}
                                        />

                                </div>


                                <div className="col-md-1">
                                    <button className="btn  btn-success btn-block" onClick={this.handleSubmit} >Filtrar</button>
                                </div>


                                </div>  

                            <ProyectosActivos proyectos={this.state.activosProyectos} />

                         </Tab>

                        <Tab eventKey={3} title="Stand By">
                           
                            <br />
                            <ProyectosInactivos proyectos={this.state.inactivosProyectos} />
                            

                        </Tab>

                        <Tab eventKey={4} title="No se Contruye">

                            <br />
                            <ProyectosInactivos proyectos={this.state.inactivosProyectos} />


                        </Tab>

                        <Tab eventKey={2} title="Terminados">

                            <br />
                            
                            <ProyectosInactivos proyectos={this.state.inactivosProyectos} />


                        </Tab>


                        

                    </Tabs>
                    
                     
                                       
                                              
                          
                           

                    
                        
                </div>

            

                
                
                
                                 
                
                
            </div>

        );
    }

}


export default ProyectosStatus;