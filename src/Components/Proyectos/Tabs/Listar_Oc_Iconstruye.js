import React from "react";
import { Table } from "react-bootstrap";
import Moment from 'react-moment';
import 'moment-timezone';


class ListTable extends React.Component {



    render() {

        const { oci } = this.props;


        return (
            <tr>

                <td>{oci.IDOC}</td>
                <td><Moment format="YYYY/MM/DD">{oci.FECHACREACION}</Moment></td>
                <td>{oci.NUMOC}</td>
                <td>{oci.NOMOC}</td>
                <td>{oci.total}</td>
                <td>{oci.descuentos}</td>
                <td>{oci.total_final}</td>

            </tr>

        );
    }
}

export default class ListOcIconstruye extends React.Component {


    render() {

        const { ocicons, totalnv } = this.props;


        if (ocicons.length === 0) {

            return (
                <div>No existen oc en iconstruye</div>
            )

        }


        const createProyecto = (pro, index) => 
        {
            return <ListTable key={index} oci={pro} />
        }

        return (



            <Table striped bordered>
                <thead>
                    <tr>
                        <th className="text-center">IDOC</th>
                        <th>FECHACREACION</th>
                        <th>NUMOC</th>
                        <th>NOMOC</th>
                        <th>TOTAL</th>
                        <th>DESCUENTOS</th>
                        <th>TOTAL FINAL</th>
                    </tr>
                </thead>
                <tbody>

                    {ocicons.map(createProyecto)}

                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>TOTAL</td>
                        <td>{totalnv.monto}</td>
                    </tr>

                </tbody>

            </Table>




        )

    }



}
