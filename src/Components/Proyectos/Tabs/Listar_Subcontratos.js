import React from "react";
import { Table } from "react-bootstrap";
import Moment from 'react-moment';
import 'moment-timezone';


class ListTable extends React.Component {



    render() {

        const { sub } = this.props;

        

        return (
            <tr>

                <td>{sub.IDDOC}</td>
                <td><Moment format="YYYY/MM/DD">{sub.FECHAINICIO}</Moment></td>
                <td>{sub.NUMDOC}</td>
                <td>{sub.NOMDOC}</td>
                <td>{sub.MONTONETO}</td>
                <td>{sub.MONTODESCUENTO}</td>
                <td>{sub.total_final}</td>

            </tr>

        );
    }
}

export default class ListSubcontratos extends React.Component {
    

    render() {

        const { subcontratos, totalnv } = this.props;


        if (subcontratos.length === 0) {

            return (
                <div>No existen oc en iconstruye</div>
            )

        }


        const createProyecto = (pro, index) => 
        {
            return <ListTable key={index} sub={pro} />
        }

        return (

            
            <Table striped bordered>
                <thead>
                    <tr>
                        <th className="text-center">IDOC</th>
                        <th>FECHACREACION</th>
                        <th>NUMOC</th>
                        <th>NOMOC</th>
                        <th>TOTAL</th>
                        <th>DESCUENTOS</th>
                        <th>TOTAL FINAL</th>
                    </tr>
                </thead>
                <tbody>

                    {subcontratos.map(createProyecto)}

                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>TOTAL</td>
                        <td>{totalnv.monto}</td>
                    </tr>

                </tbody>

            </Table>




        )

    }



}
