import React from "react";
import {Table} from "react-bootstrap";
import Moment from 'react-moment';
import 'moment-timezone';


class ListTable extends React.Component {



    render() {

        const { ocs } = this.props;


        return (
            <tr>

                <td>{ocs.NumOC}</td>
                <td><Moment format="YYYY/MM/DD">{ocs.FechaOC}</Moment></td>
                <td>{ocs.CodAux}</td>
                <td>{ocs.NomAux}</td>
                <td>{ocs.proyecto}</td>
                <td>{ocs.NomCon}</td>
                <td>{ocs.ObservOC}</td>
                <td>{ocs.totalExcento}</td>
                <td>{ocs.NetoAfecto}</td>
                <td>{ocs.NetoExento}</td>
                <td>{ocs.ValorTotOC}</td>

            </tr>

        );
    }
}

export default class ListOcSoftland extends React.Component {
    

    render() {

        const { ocsoftl, totalnv } = this.props;


        if (ocsoftl.length === 0) {

            return (
                <div>No existen oc en iconstruye</div>
            )

        }

        const createProyecto = (pro, index) => 
        {
            return <ListTable key={index} ocs={pro} />
        }

        return (


            <Table striped bordered>
                <thead>
                    <tr>
                        <th className="text-center">NumOC</th>
                        <th>FechaOC</th>
                        <th>CodAux</th>
                        <th>NomAux</th>
                        <th>proyecto</th>
                        <th>NomCon</th>
                        <th>ObservOC</th>
                        <th>totalExcento</th>
                        <th>NetoAfecto</th>
                        <th>NetoExento</th>
                        <th>ValorTotOC</th>
                    </tr>
                </thead>
                <tbody>

                    {ocsoftl.map(createProyecto)}

                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>TOTAL</td>
                        <td>{totalnv.monto}</td>
                    </tr>

                </tbody>

            </Table>




        )

    }



}
