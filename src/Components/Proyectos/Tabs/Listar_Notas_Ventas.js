import React from "react";
import {  Table } from "react-bootstrap";
import Moment from 'react-moment';
import 'moment-timezone';



class ListTable extends React.Component {


    
    render() {

        const { nv } = this.props;
        

        return (
            <tr>

               
                <td>{nv.nvnumero}</td>
                <td><Moment  format="YYYY/MM/DD">{nv.nvfem}</Moment></td>
                <td>{nv.codaux}</td>
                <td>{nv.neto}</td>
                <td>{nv.monto}</td>

            
              

            </tr>
           
        );
    }
}

export default class ListNotasVentas extends React.Component
{
   


    render()
    {

        const { notasdeventa, totalnv } = this.props;


        if (notasdeventa.length === 0) {

            return (
                <div>No existen notas de venta</div>
            )

        }


        const createProyecto = (pro , index) => 
        {
            return <ListTable key={index} nv={pro} />
        }

        return(


            <Table striped bordered>
                <thead>
                    <tr>
                        <th className="text-center">Numero</th>
                        <th>Fecha</th>
                        <th>Codaux</th>
                        <th>Neto</th>
                        <th>Monto Total</th>
                       
                    </tr>
                </thead>
                <tbody>

                    {notasdeventa.map(createProyecto)} 


                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>TOTAL</td>
                        <td>{totalnv.monto}</td>
                    </tr>

                </tbody>

            </Table>


            
           
        )

    }



}
