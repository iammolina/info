import React from 'react';


const GithubMenuItem = ({ user }) => (
    <div>
        <span><strong>{ user.centro_costo }</strong> - {user.cc_nombre_proyecto}</span>
    </div>
);

export default GithubMenuItem;