import React, { Component } from 'react';
import BarraNavegacion from './BarraNavegacion';
import GithubMenuItem from "../Components/GithubMenuItem"
import CurrencyInput from 'react-currency-input';
import Money from 'react-money-component';
import { getFacturar, getFacturasSolicitadas, addSolicitud, putFacturarEstado} from "../Api/facturar";
import { getFind } from "../Api/centrocosto";
import { AsyncTypeahead } from 'react-bootstrap-typeahead';
import { Tabs, Tab, Table, Button, Modal, DropdownButton, MenuItem, Alert} from "react-bootstrap";
import { model_facturar } from '../Schemas/Factura';
import  UpdateFactura  from '../Forms/UpdateFactura';
import AceptarFactura from '../Forms/AceptarFactura';


class ListTableFacturado extends React.Component 
{
    constructor(props) {
        super(props);

        this.state = {
            model: model_facturar,
        }
    }

    render()
    {
        const { fact_solicitadas  } = this.props;
        return (
            <tr>
                <td>{fact_solicitadas.cc}</td>
                <td>{fact_solicitadas.nombre}</td>
                <td>{fact_solicitadas.f_solicita}</td>
                <td>{fact_solicitadas.id_realizado}</td>
                <td>{fact_solicitadas.f_realizado}</td>
                <td>{fact_solicitadas.num_factura}</td>
                <td>{fact_solicitadas.detalle}</td>
            </tr>

        );
    }
}




class ListTable extends React.Component 
{

    constructor(props) 
    {
        super(props);

        this.state = {
            model: model_facturar,

        }
    }

    render() {

        const { fact_solicitadas, onAdjunto , onUpdate , onOk , onDelete  } = this.props;
        return (
            <tr>
                <td>{fact_solicitadas.cc}</td>
                <td>{fact_solicitadas.nombre}</td>
                <td>{fact_solicitadas.f_solicita}</td>
                <td><Money cents={fact_solicitadas.monto} currency="CLP" /> </td>
                <td>{fact_solicitadas.oc}</td>
                <td>{fact_solicitadas.detalle}</td>
                <td>
                    <DropdownButton
                        bsStyle="default"
                        title="opciones"
                        key={fact_solicitadas.id}
                        id={fact_solicitadas.id}
                    >
                        <MenuItem eventKey="1" onClick={onUpdate} >Editar</MenuItem>
                        <MenuItem eventKey="2" onClick={onAdjunto} >Adjunto</MenuItem>
                        <MenuItem eventKey="3" onClick={onDelete}>Eliminar</MenuItem>
                        <MenuItem eventKey="3" onClick={onOk}>Aceptar</MenuItem>
                    </DropdownButton>
                </td>
            </tr>

        );
    }
}

class Facturar extends Component 
{
    constructor(props) 
    {
        super(props)

        this.state = {
            isLoading: false,
            centro_costo : 0,
            options: [],
            facturas_solicitadas : [],
            montos : [],
            m_facturar : 0,
            porcentaje: 0,
            file :null,
            cc:0 ,
            show : false,
            mensaje_text : '',
            mensaje_show : false,
            mensaje_class : 'defualt',
            show_update : false,
            show_ok: false,
            show_delete: false,
            facturaID : null,
            lista_facturado : []
        }
     
        this.handleUpload = this.handleUpload.bind(this);
        this._totalmonto = this._totalmonto.bind(this);
        this._showMensaje = this._showMensaje.bind(this);
        this._showUpdate = this._showUpdate.bind(this);
        this._showOk = this._showOk.bind(this);
        this._showDelete = this._showDelete.bind(this);
        this.handleClose = this.handleClose.bind(this);
        this.handleSelect = this.handleSelect.bind(this);
        this._eliminarSolicitud = this._eliminarSolicitud.bind(this);
    }

    componentDidMount()
    {
        this._index(1);
    }
    
    _CC(e)
    {
        if(e[0])
        {
            getFacturar(e[0].centro_costo)
                .then((response) => 
                {

                    if (response.data.mnnv > 0)
                    {
                        this.setState({
                            cc: e[0].centro_costo,
                            montos: response.data,
                            show: true
                        });
                    }else
                    {
                        this.setState({
                            show: false,
                        });

                        this._showMensaje('ALERTA - El centro costo no tiene notas de venta', 'danger');
                    }

                }).catch((error) => 
                {
                    alert('error');
                });
        }else
        {


        }
    }


    handleSelect(e)
    {

        this._index(e);

    }

    _showMensaje(text, css )
    {

        this.setState({
            mensaje_show : true,
            mensaje_class : css,
            mensaje_text : text
        });


        setTimeout(() => 
        {
            this.setState({
                mensaje_show: false
            });
            
        }, 5000);

    }

    _handleSearch = (query) => {
        this.setState({ isLoading: true });
        var datos = { data: query };

        getFind(datos)
            .then((response) => 
            {
                this.setState({
                    isLoading: false,
                    options: response.data.items
                });
            })
    }

    _totalmonto(e)
    {

        var monto_pendiente = this.state.montos.pen;
        var monto_facturar = ((e.target.value * monto_pendiente) / 100);
        if (monto_facturar > monto_pendiente)
        {
            alert('lo facturado es mayor a lo pendiente');
        }
        this.setState({
            porcentaje: e.target.value,
            m_facturar: monto_facturar,
        });
    }





    handleUpload(ev) 
    {
        const data = new FormData();
        data.append('file', this.uploadInput.files[0]);
        data.append('porcentaje', this.state.porcentaje);
        data.append('monto_facturar', this.state.m_facturar);
        data.append('Numoc', this.numOc.value);
        data.append('detalle', this.detalle.value);
        data.append('cc', this.state.cc);

        addSolicitud(data).then((response) => 
        {
            this.myFormRef.reset();

            this.setState({ m_facturar: 0, show: false })

            this._showMensaje('Se ha creado la solicitud de facturacion , se enviara un correo de configuracion ...', 'success');

            this._index(1);

        }).catch((error) => {
            alert('error');
        });

        ev.preventDefault();
    }

    _onAdjuntos(id)
    {
        window.open('http://localhost:5000/oc/' + id + '.jpg' , '_blank');
    }

    _showUpdate(fact)
    {

        this.setState({ facturaID : fact  , show_update: true });

    }

    handleClose()
    {
        this.setState({ show_update: false  , show_ok : false , show_delete : false } );
        this._index(1);
    }


    _showOk(e)
    {
        this.setState({ facturaID : e ,  show_ok : true });
    }


    _showDelete(e) 
    {
        this.setState({ facturaID: e, show_delete: true });

    }

    _eliminarSolicitud()
    {

        console.log(this.state.facturaID);

        const obj = { id : this.state.facturaID.id , estado : 3 };

        putFacturarEstado(obj)
            .then((response) => 
            {
                
                this.handleClose();

            }).catch((error) => {
                alert('error');
            });
    }


    _index(id) 
    {

        getFacturasSolicitadas(id).then((response) => 
        {
            if(id === 1)
            {
                this.setState({ facturas_solicitadas: response.data })
            }

            if(id === 2)
            {
                this.setState({ lista_facturado : response.data })
            }
            

        }).catch((error) => {
            alert('error');
        });

    }

    render() {

        const { facturas_solicitadas, montos, show, mensaje_text, mensaje_show, mensaje_class, lista_facturado } = this.state;

        var porcentaje_facturado = Math.round(((montos.mf * 100) / montos.mnnv));
        var porcentaje_pendiente = 100 - porcentaje_facturado;

        

        const createTable_facturasSolicitadas = (fact, index) => {
            return <ListTable key={index} fact_solicitadas={fact} onAdjunto={() => this._onAdjuntos(fact.id)} onUpdate={() => this._showUpdate(fact)} onOk={() => this._showOk(fact)} onDelete={() => this._showDelete(fact)} />
        }

        const createTable_facturado = (fact, index) => 
        {
            return <ListTableFacturado key={index} fact_solicitadas={fact} onAdjunto={() => this._onAdjuntos(fact.id)} />
        }


        return(

            <div>

              <BarraNavegacion />


             
               <div className="container">

                    <Tabs id="" key={1} onSelect={this.handleSelect} >

                        <Tab eventKey={1} title="Solicitud">

                            <br/>

                            {mensaje_show &&

                                <Alert bsStyle={mensaje_class}>
                                    <h4>{mensaje_text}</h4>
                                </Alert>

                            }

                            <h1>Solicitud Facturación</h1>

                            <div class="row">

                                <div className="col-md-4">
                                   
                                        <AsyncTypeahead

                                            {...this.state}
                                            labelKey="cc"
                                            onSearch={this._handleSearch}
                                            onChange={(e) => 
                                            {
                                                this._CC(e)
                                            }}
                                            placeholder="Centro Costo ..."
                                            renderMenuItemChildren={(option, props) =>
                                            (
                                                <GithubMenuItem user={option} />
                                            )}
                                        />

                                </div>

                                <div class="col-md-8">

                                    
                                    { show &&
                                    <form ref={(el) => this.myFormRef = el} onSubmit={this.handleUpload}>
                                            
                                            <div className="form-group">
                                                <input className="form-control" onBlur={this._totalmonto} type="text" placeholder="% a facturar" />
                                            </div>

                                            <div className="form-group">
                                                <CurrencyInput className="form-control" value={this.state.m_facturar} decimalSeparator="," thousandSeparator="." precision="0"  disabled />
                                            </div>

                                            <div className="form-group">
                                                <input className="form-control" ref={(ref) => { this.uploadInput = ref; }} type="file" />
                                            </div>

                                            <div className="form-group">
                                                <input className="form-control" ref={(ref) => { this.numOc = ref; }} type="text" placeholder="Numero OC" />
                                            </div>


                                            <div className="form-group">

                                            <textarea className="form-control" ref={(ref) => { this.detalle = ref; }} placeholder="Detalle"></textarea>

                                            </div>


                                        <button className="btn btn-success pull-right" type>Aceptar</button>

                                    </form>
                                    }

                                </div>

                            </div>
                            <br />

                            {show &&

                            <Table striped bordered>
                                <thead>
                                    <tr>
                                        <th className="text-center">Monto Nota de Venta</th>
                                        <th colSpan={2}>Facturado</th>
                                        <th colSpan={2}>Pendiente</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    {montos.mnnv > 0 &&

                                    <tr>
                                        <td><Money cents={montos.mnnv} currency="CLP" /></td>
                                        <td><Money cents={montos.mf} currency="CLP" /> </td>
                                        <td>{porcentaje_facturado}%</td>
                                        <td><Money cents={montos.pen} currency="CLP" /> </td>
                                        <td>{porcentaje_pendiente}%</td>
                                    </tr>
                                    }


                                </tbody>


                            </Table>
                            }



                            <br/>
                            <Table striped bordered>
                                <thead>
                                    <tr>
                                        <th className="text-center">CC</th>
                                        <th>Solicitado por</th>
                                        <th>Fecha Solicitud</th>
                                        <th>MONTO</th>
                                        <th>OC</th>
                                        <th>Detalle</th>
                                        <th>Opciones</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    {facturas_solicitadas.map(createTable_facturasSolicitadas)} 

                                </tbody>




                                <Modal show={this.state.show_update} onHide={this.handleClose}>
                                    <Modal.Header closeButton>
                                        <Modal.Title>Actualizar Solicitud de facturación</Modal.Title>
                                    </Modal.Header>
                                    <Modal.Body>


                                        <UpdateFactura fact={this.state.facturaID} />
                                      

                                    </Modal.Body>
                                    <Modal.Footer>
                                        <Button onClick={this.handleClose}>Cerrar</Button>
                                    </Modal.Footer>
                                </Modal>



                                <Modal show={this.state.show_delete} onHide={this.handleClose}>
                                    <Modal.Header closeButton>
                                        <Modal.Title>Eliminar Solicitud de facturación</Modal.Title>
                                    </Modal.Header>
                                    <Modal.Body>

                                        <h1 className="text-center">¿Deseas eliminar la solicitud?</h1>


                                    </Modal.Body>
                                    <Modal.Footer>
                                        <Button bsStyle="success" onClick={this._eliminarSolicitud}>Si</Button>
                                        <Button onClick={this.handleClose}>No</Button>
                                    </Modal.Footer>
                                </Modal>


                                <Modal show={this.state.show_ok} onHide={this.handleClose}>
                                    <Modal.Header closeButton>
                                        <Modal.Title>Aceptar Solicitud de facturación</Modal.Title>
                                    </Modal.Header>
                                    <Modal.Body>

                                          <AceptarFactura fact={this.state.facturaID} />

                                    </Modal.Body>
                                    <Modal.Footer>
                                        <Button onClick={this.handleClose}>Cerrar</Button>
                                    </Modal.Footer>
                                </Modal>



                            </Table>

                            
                            
                        </Tab>

                        <Tab eventKey={2} title="Facturado">

                         <br/>
                            <Table striped bordered>
                                <thead>
                                    <tr>
                                        <th className="text-center">CC</th>
                                        <th>Solicitado por</th>
                                        <th>Fecha Solicitud</th>
                                        <th>Facturado por</th>
                                        <th>Fecha Factura</th>
                                        <th>Numero Factura</th>
                                        <th>Detalle</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    {lista_facturado.length > 0 &&
                                        lista_facturado.map(createTable_facturado)
                                    }

                                </tbody>



                            </Table>

                        </Tab>

                    </Tabs>

                </div>

            </div>

         
           

        )

   
    }

}


export default Facturar;