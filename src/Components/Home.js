import React, { Component } from 'react';
import jwtDecode from 'jwt-decode';
import BarraNavegacion from './BarraNavegacion';
import ProyectosHome from './Proyectos/ProyectosHome';
import ProyectosValidar from'./Proyectos/ProyectosValidar';
import ProyectosStatus from './Proyectos/ProyectosStatus';
import { Redirect } from 'react-router';
import { Tab, Tabs} from 'react-bootstrap';
import { getCC, getCCValidar } from '../Api/centrocosto';


class Home extends Component 
{

    constructor(props, context) 
    {
        super(props, context);

        this.state =
            {
                proyectos: [],
                proyectos_validar : [],
                token: 'Bearer ' + localStorage.getItem('user'),
                user: JSON.parse(localStorage.getItem('user'))

            }

        this.handleSelect = this.handleSelect.bind(this);
    }


    componentDidMount() 
    {
            getCC(this.state.token).then((response) => 
            {
                this.setState({
                    proyectos: response.data
                })
            })
            .catch((error) => {

                console.log(error);

            });
    }


    handleSelect(key) 
    {
       if(key === 2)
       {

           getCCValidar().then((response) => 
           {
               
               this.setState({
                   proyectos_validar : response.data
               })
           })
               .catch((error) => {

                   alert('error');

               });

       }
    
    }


    render() 
    {   
        if(!this.state.user)
        {
            return <Redirect to="/login" />;
        }


        const token = jwtDecode(this.state.user);
        var user = token[0];


        return (

            <div>

                <BarraNavegacion />

                <Tabs id="" key={1} onSelect={this.handleSelect} >

                    <Tab eventKey={1} title="Proyectos">


                        {user.ver_todos_proyectos === 1 && 

                          <ProyectosStatus/>

                        }

                        {user.ver_propios_proyectos === 1 &&

                            <ProyectosHome proyectos={this.state.proyectos} />

                        }

                        {user.ver_areas_proyectos === 1 &&

                            <ProyectosHome proyectos={this.state.proyectos} />
                        }


                    </Tab>


                    {user.validar_cc === 1 &&

                        <Tab eventKey={2} title="Validar Centro Costos">
                            <ProyectosValidar proyectos={this.state.proyectos_validar} select={() => this.handleSelect(2)} />
                        </Tab>

                    }

                    

               </Tabs>

            </div>

               
           
        );
    }
}

export default Home;

