import React from 'react';

const UserMenuItem = ({ user }) => (
    <div>
        <span><strong>{user.nombre} {user.apellido}</strong></span>
    </div>
);

export default UserMenuItem;