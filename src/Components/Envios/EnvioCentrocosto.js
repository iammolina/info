import Form from 'react-formal'
import React, { Component } from 'react'
import UserMenuItem from "../UserMenuItem";
import { modelSchema } from "../../Schemas/EnvioCentroCosto"
import { AsyncTypeahead } from 'react-bootstrap-typeahead'
import { Redirect } from 'react-router-dom';
import { SendCc , findUsersCc , findUsers } from '../../Api/centrocosto';



class EnvioCentroCosto extends Component {

    constructor() {
        super();
        this.state = {
            model: modelSchema,
            isLoading: false,
            options: [],
            mail: null,
            _para: [],
            _CC: [],
            _CCO:[],
            isRedirect : false,
            num:0
        }
    }


    componentDidMount()
    {
        var id = this.props.id;

        findUsersCc(id)
            .then((response) => {
                this.setState({
                    _para: [response.data.items[0].email_j_area, response.data.items[0].email_j_proyecto ]
                });
            })
    }

  

    _addMail(e)
    {
        
        const _CCArray = [];
        const _CCOArray = [];

        var id = this.props.id;

        const { _CC , _CCO , _para } = this.state;

        for (let index = 0; index < _CC.length; index++)
        {
            _CCArray.push(_CC[index].correo);
        }

        for (let index = 0; index < _CCO.length; index++) 
        {
            _CCOArray.push(_CCO[index].correo);
        }

        const array = { para : _para , cc : _CCArray , cco : _CCOArray , comentario : e.mail.comentario };

        
        SendCc(id , array )
            .then((response) => {
                alert('Email Enviado ...');
                this.setState({
                     isRedirect : true,
                     num: id
                });
            })
    }


    _handleSearch = (query) => {
        this.setState({ isLoading: true });

        var datos = { data: query };

        findUsers(datos)
            .then((response) => {
                this.setState({
                    isLoading: false,
                    options: response.data.items
                });

            })
    }


    render() {

        const { isRedirect  ,  model   , _para  , num } = this.state;
 

        if (isRedirect) {

            var path = '/proyecto/' + num;
            return <Redirect to={path} />;

        }


        return (

            <div className="container-fluid">

                <Form
                    onSubmit={(e) => this._addMail(e)}
                    schema={modelSchema}
                    value={model}
                    onChange={modelInfo => this.setState({ model: modelInfo })}
                >
                    <div className="clearfix"></div>
            

                    <div style={{ "marginTop": "10px" }} className="form-group">
                        <label>Para</label>
                        <div>
                            <AsyncTypeahead
                                {...this.state}
                                labelKey="nombre_usuario"
                                minLength={3}
                                multiple
                                selected={_para}
                                onSearch={this._handleSearch}
                                disabled
                                placeholder="Buscar el jefe de proyecto ..."
                                renderMenuItemChildren={(option, props) => (
                                    <UserMenuItem key={option.id} user={option} />

                                )}
                            />

                        </div>
                        
                    </div>


                    <div style={{ "marginTop": "10px" }} className="form-group">
                        <label>CC</label>
                        <div>
                            <AsyncTypeahead
                                {...this.state}
                                labelKey="nombre_usuario"
                                minLength={3}
                                multiple
                                onSearch={this._handleSearch}
                                onChange={(selected) => {
                                    this.setState({ _CC : selected });
                                }}
                                placeholder="Buscar el jefe de proyecto ..."
                                renderMenuItemChildren={(option, props) => (
                                    <UserMenuItem key={option.id} user={option} />

                                )}
                            />

                        </div>

                    </div>

                    <div style={{ "marginTop": "10px" }} className="form-group">
                        <label>CCO</label>
                        <div>
                            <AsyncTypeahead
                                {...this.state}
                                labelKey="nombre_usuario"
                                minLength={3}
                                multiple
                                onSearch={this._handleSearch}
                                onChange={(selected) => {

                                    this.setState({ _CCO: selected });

                                }}
                                placeholder="Buscar el jefe de proyecto ..."
                                renderMenuItemChildren={(option, props) => (
                                    <UserMenuItem key={option.id} user={option} />

                                )}
                            />

                        </div>

                    </div>

                    <div className="clearfix"></div>

                    <div style={{ "marginTop": "10px" }} className="form-group">

                        <label>Comentario:</label>

                        <Form.Field type="textarea" className={`form-control col-md-12`} rows="5" name='mail.comentario' placeholder='Escriba un comentario ...' />
                        <Form.Message errorClass={`col-md-12 alert alert-danger`} for='mail.comentario' />

                    </div>

                    <div className="clearfix"></div>

                    <Form.Button style={{ "marginTop": "10px" }} className={`btn btn-block btn-success`} type='submit'>Enviar</Form.Button>

                </Form>

              
            </div>

        )
    }

}


export default EnvioCentroCosto;
