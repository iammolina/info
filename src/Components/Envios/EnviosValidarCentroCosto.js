import React, { Component } from 'react'
import BarraNavegacion from "../BarraNavegacion";



class EnvioValidar extends Component {

   
    render() {

        const id = this.props.match.params.id;

        return (

            <div className="container">

                <BarraNavegacion />

                <div class="text-center">

                    <img  src="/mail.png" alt="thumbnail" />

                    <h1>El centro de costo se envio a validacion</h1>

                    <h1><small>Una vez validado le llegara un mail de confirmacion , puede consultar con el siguiente:#<strong>{id}</strong></small></h1>


                    <a class="btn btn-info" href="/">ir al home</a>

                </div>


            </div>

        )
    }

}


export default EnvioValidar;
