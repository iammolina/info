import jwtDecode from 'jwt-decode';
import React , {Component} from 'react';
import { Redirect } from 'react-router';
import { getFind  } from "../Api/centrocosto";
import { NavItem, Navbar, Nav, NavDropdown } from "react-bootstrap";
import '../App.css';

class BarraNavegacion extends Component
{
    constructor(props)
    {
        super(props)
        
        this.state = {
            isLoading: false,
            isRedirect: false,
            isRedirectLogin: false,
            num: 0,
            options: [],
            user: JSON.parse(localStorage.getItem('user'))
        }
        
        this.onLogout = this.onLogout.bind(this);
    }

    componentDidMount()
    {
        if(!localStorage.user)
        {
            this.setState({ isRedirectLogin: true });
        }

    }

    _jefeProyecto(e) 
    {
        if (e[0]) 
        {
            this.setState({ isRedirect: true  , num : e[0].id_proyecto });
        } 
    }


    onLogout()
    {
        this.setState( {isRedirectLogin : true} );
        window.localStorage.clear();
    }



    _handleSearch = (query) => {
        this.setState({ isLoading: true });

        var datos = { data: query };

        getFind(datos)
            .then((response) => {

                this.setState({
                    isLoading: false,
                    options: response.data.items
                });
             
            })
    }



    render(){

        const { isRedirect ,isRedirectLogin , num } = this.state;

        var u = {};


        if (this.state.user)
        {
            const token = jwtDecode(this.state.user);
            u = token[0];
        }


        if (isRedirect) {

            var path = '/proyecto/' + num;
            return <Redirect to={path} />;

        }

        if (isRedirectLogin) 
        {
            return <Redirect to="/login" />;
        }

        return(

            <div>

            <Navbar fixedTop inverse collapseOnSelect>
                <Navbar.Header>
                    <Navbar.Brand>
                        <a href="/">INFO</a>
                    </Navbar.Brand>
                </Navbar.Header>
                <Nav>

                    <NavItem eventKey={1} href="/add/">
                        Crear CC
                    </NavItem>

                    
                    <NavItem eventKey={3} href="/facturar/">
                            Facturar
                    </NavItem>
                   
                </Nav>

                {this.state.user &&

                <Nav pullRight>
                        <NavDropdown eventKey={3} title={u.correo} id="basic-nav-dropdown">
                            <NavItem eventKey={3} onClick={this.onLogout}>
                                Salir
                            </NavItem>
                        </NavDropdown>
                </Nav>}
            </Navbar>

            <br/>
                <br />
                <br />
                <br />

            

            </div>

        )
    }

}


export default BarraNavegacion;