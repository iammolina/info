import React from 'react';
import ReactDOM from 'react-dom';

import 'bootstrap/dist/css/bootstrap.css';
import  'react-bootstrap-typeahead/css/Typeahead.css';
import './index.css';
import 'moment/locale/es';


import App from './Components/Crear_centro_costo';
import Home from './Components/Home';
import ProyectoID from './Components/Proyectos/ProyectoId';
import EnvioValidar from './Components/Envios/EnviosValidarCentroCosto';
import ProyectosStatus from './Components/Proyectos/ProyectosStatus';
import Facturar from './Components/Facturar';
import Login from './Components/Login';
import Page404 from './Components/Page404';


import registerServiceWorker from './registerServiceWorker';
import { BrowserRouter as Router, Route, Switch  } from "react-router-dom";



ReactDOM.render(
    <Router>
        <Switch>
            <Route exact path='/' component={Home} />
            <Route path='/add/' component={App} />
            <Route path='/proyecto/:id' component={ProyectoID} />
            <Route path='/status' component={ProyectosStatus} />
            <Route path='/login' component={Login} />
            <Route path='/facturar' component={Facturar} />
            <Route path='/envio/validar/:id' component={EnvioValidar} />
            <Route component={Page404} />
        </Switch>
    </Router>,

    document.getElementById('root')
);
registerServiceWorker();