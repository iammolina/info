import axios from 'axios';

var token = 'Bearer ' + localStorage.getItem('user');

var full = window.location.hostname;

var url = 'http://' + full + ':5000/';

export function getFacturar(id) 
{
    return axios.get(url + 'facturar/' + id , { headers: { 'Authorization': token } });
}


export function getFacturasSolicitadas(id)
{

    return axios.get(url + 'facturar/fact/solicitadas/' + id , { headers: { 'Authorization': token } });

}

export function addSolicitud(data) 
{

    return axios.post(url + 'facturar/fact/solicitadas', data, { headers: { 'Authorization': token, 'content-type': 'multipart/form-data' } });

}

export function putFacturar(data)
{

    return axios.put(url + 'facturar/fact/solicitadas', data , { headers: { 'Authorization': token } });

}

export function putFacturarEstado(data) {

    return axios.put(url + 'facturar/fact/solicitadas/estado', data, { headers: { 'Authorization': token } });

}

export function putFacturarAceptar(data) {

    return axios.put(url + 'facturar/fact/solicitadas/aceptar', data, { headers: { 'Authorization': token } });

}
