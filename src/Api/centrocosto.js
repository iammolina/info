
import axios from 'axios';

const token = 'Bearer ' + localStorage.getItem('user');

var full = window.location.hostname;

var url = 'http://' + full + ':5000/';


export function getCC(pass) 
{
    return axios.get( url + 'centrocosto', { headers: { 'Authorization': pass } });
}

export function addHarvest(data) 
{
    return axios.post( url + 'centrocosto/add/harvest', data , { headers: { 'Authorization': token } });
}


export function activarCC(id)
{
    return axios.get( url + 'centrocosto/activar/' + id, { headers: { 'Authorization': token } });
}

export function addSoftland(id)
{
    return axios.get( url + 'centrocosto/add/softland/' + id, { headers: { 'Authorization': token } });
}

export function emailconfirmacion(data) {
    return axios.post( url + 'centrocosto/send/emailconfirmacion', data, { headers: { 'Authorization': token } });
}


export function getCCValidar()
{
    return axios.get( url + 'centrocosto/validar/', { headers: { 'Authorization': token } });
}

export function postCc(datos)
{
    return axios.post( url + 'centrocosto/', datos, { headers: { 'Authorization': token } });
}

export function getInactivos(key)
{
    return axios.get( url + 'centrocosto/inactivos/' + key, { headers: { 'Authorization': token } });
}

export function getFiltrado(data)
{
    return axios.post( url + 'centrocosto/find/filtrado', data, { headers: { 'Authorization': token } });
}

export  function PutCc(id , data)
{

    return axios.put( url + 'centrocosto/' + id , data, { headers: { 'Authorization': token } });
    
}

export function getId(id)
{
    return axios.get( url + 'centrocosto/find/' + id + '/', { headers: { 'Authorization': token } });
}

export function getFind(datos)
{
    return axios.post( url + 'centrocosto/find/', datos, { headers: { 'Authorization': token } })
}

export function getConsultas(data)
{
    return axios.get(url + data  , { headers: { 'Authorization': token } });
}

export function getHistorial(id) 
{
    return axios.get( url + 'centrocosto/historial/' + id, { headers: { 'Authorization': token } });
}

export function postHistorial(id , hist)
{
    return axios.post( url + 'centrocosto/historial/' + id, hist, { headers: { 'Authorization': token } })   
}

export function SendCc(id , array ) 
{
 
    return axios.post( url + 'centrocosto/send/' + id, array, { headers: { 'Authorization': token } });
    
}

export function findUsersCc(id) 
{
    return axios.get( url + 'centrocosto/users/findcc/' + id, { headers: { 'Authorization': token } });

}

export function findUsers(datos) 
{
    return axios.post( url + 'centrocosto/users/find/', datos, { headers: { 'Authorization': token } });
}

export function getEmpresas(datos)
{
    return axios.post( url + 'centrocosto/empresas/', datos, { headers: { 'Authorization': token } });
    
}

