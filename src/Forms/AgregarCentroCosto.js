import Form from 'react-formal'
import jwtDecode from 'jwt-decode';
import React, { Component } from 'react'
import fecha_moment from 'moment/moment';
import UserMenuItem from "../Components/UserMenuItem"
import EmpresaMenuItem from "../Components/EmpresaMenuItem"
import { Redirect } from 'react-router-dom';
import { model_CentroCosto } from "../Schemas/CrearCentroCosto"
import { AsyncTypeahead } from 'react-bootstrap-typeahead'
import { getEmpresas, postCc, findUsers } from '../Api/centrocosto';


class AgregarCentroCosto extends Component 
{
    constructor()
    {
        super();
        this.state = {
            model: model_CentroCosto,
            isLoading: false,
            options: [],
            cc:{},
            jefe_proyecto : ' ',
            _empresa: '',
            redirect : false,
            id:0,
            error: false,
            user: JSON.parse(localStorage.getItem('user'))
        }

    }


    _jefeProyecto(e)
    {

        if(e[0])
        {
            this.setState({ jefe_proyecto:  e[0].id_usuario });

        }else
        {
            this.setState({ jefe_proyecto : '' });
        }

    }


    _emp(e) {
        if (e[0]) {
            this.setState({ _empresa : e[0].id_harvest });

        } else {
            this.setState({ _empresa : '' });
        }
    }


    _jefeArea(e) {
        if (e[0]) {
            this.setState({ jefe_area: e[0].id_usuario });


        } else {
            this.setState({ jefe_area : '' });
        }

    }


    Create_cc(e)
    {
        const user = jwtDecode(this.state.user);

        var a = 
        { 
            cc_empresa: this.state._empresa,
            cc_jefe_proyecto: this.state.jefe_proyecto, 
            cc_jefe_area: user[0].id_usuario, 
            cc_fecha_inicio : fecha_moment(e.cc.cc_fecha_inicio).format('X'),
            cc_fecha_termino : fecha_moment(e.cc.cc_fecha_termino).format('X'),
            cc_fecha_presupuesto: fecha_moment(e.cc.cc_fecha_presupuesto).format('X'),
            cc_fecha_planimetria: fecha_moment(e.cc.cc_fecha_planimetria).format('X'),
            estado:0,
            fase: 1,
            create_at : (Date.now()/1000) 
        }

        var b = { ...e.cc, ...a };

        var datos = { data : b};
        
        postCc(datos)
        .then(( response ) => {

            if (response) 
            {
                this.setState({
                    redirect: true,
                    id: response.data.data
                });
            }
            else
            {
                this.setState({
                    error: true
                })

            }


        })
        .catch((error) => {
                this.setState({
                    error: true
                })
            })


    }

    _handleSearchEmpresa = (query) => 
    {
        this.setState({ isLoading: true });
        var datos = { data: query };
       
        getEmpresas(datos)
            .then((response) => {
                this.setState({
                    isLoading: false,
                    options: response.data
                });

            })
    }
    

    _handleSearch = (query) => {
        this.setState({ isLoading: true });

        var datos = { data : query };

        findUsers(datos) 
            .then((response) => {
                this.setState({
                    isLoading: false,
                    options: response.data.items
                });

            }) 
    }



    render() {

        const { error, id , redirect ,model} = this.state;

        if (redirect) {
            var path = '/envio/validar/' + id;
            return <Redirect to={path} />;
        }

        return (

            <div>
           
                <h1 className="text-center">Solicitud de creación de centro de costo</h1>
              
                <Form
                    onSubmit={(e) => this.Create_cc(e)}
                    schema={model_CentroCosto}
                    value={model}
                    onChange={modelInfo => this.setState({ model: modelInfo })}
                 >

                    {error === true &&
                        <div className="alert alert-danger">
                            A ocurrirdo un error vuelva a intentarlo , si persiste el problema comuniquese con el administrador.
                        </div>
                    }

                    <div className="row">
                        <div className="col-md-6">

                            <div style={{ "marginTop": "10px" }} className="form-group">
                                <label>Nombre Proyecto</label>
                                <Form.Field className={`form-control col-md-12`} name='cc.cc_nombre_proyecto' placeholder='Descripcion' />
                                <Form.Message errorClass={`col-md-12 alert alert-danger`} for='cc.cc_nombre_proyecto' />
                            </div>
                            
                            </div>

                        <div className="col-md-6">
                            <div style={{ "marginTop": "10px" }} className="form-group">
                                <label>Ubicacion</label>
                                <Form.Field className={`form-control col-md-12`} name='cc.cc_ubicacion' placeholder='Descripcion' />
                                <Form.Message errorClass={`col-md-12 alert alert-danger`} for='cc.cc_ubicacion' />
                            </div>
                        </div>
                    </div>


                    <div className="clearfix"></div>


                    <div style={{ "marginTop": "10px" }} className="form-group">
                        <label>Empresa / Cliente</label>

                        <div>
                            <AsyncTypeahead
                                {...this.state}
                                labelKey="name"
                                minLength={3}
                                onSearch={this._handleSearchEmpresa}
                                onChange={(e) => {
                                    this._emp(e)
                                }}
                                placeholder="Buscar la empresa ..."
                                renderMenuItemChildren={(option, props) => (
                                    <EmpresaMenuItem key={option.id} emp={option} />

                                )}
                            />

                        </div>

                        <Form.Message errorClass={`col-md-12 alert alert-danger`} for='cc.cc_jefe_area' />

                    </div>


                    <div className="clearfix"></div>

                    <div style={{ "marginTop": "10px" }} className="form-group">
                        <label>Project Manager / Cliente</label>
                        <Form.Field className={`form-control col-md-12`} name='cc.cc_cliente' placeholder='Descripcion' />
                        <Form.Message errorClass={`col-md-12 alert alert-danger`} for='cc.cc_cliente' />
                    </div>

                    <div className="clearfix"></div>

                    <div style={{ "marginTop": "10px" }} className="form-group">
                        <label>Direccion</label>
                        <Form.Field className={`form-control col-md-12`} name='cc.cc_direccion' placeholder='Descripcion' />
                        <Form.Message errorClass={`col-md-12 alert alert-danger`} for='cc.cc_direccion' />
                    </div>

                    <div className="clearfix"></div>

                    <div style={{ "marginTop": "10px" }} className="form-group">
                        <label>Superficie [m2]</label>
                        <Form.Field className={`form-control col-md-12`} name='cc.cc_superficie' placeholder='Descripcion' />
                    </div>

                    <div className="clearfix"></div>

                    <div style={{ "marginTop": "10px" }} className="form-group">
                        <label>Modelo de  negocio</label>
                        <Form.Field className={`form-control col-md-12`} name='cc.cc_unidad_negocio' type='select'>
                            <option value={null}>Seleccionar área o unidad de negocio...</option>
                            <option value={1}>ADMINISTRACIÓN INSTORE</option>
                            <option value={2}>CORNER</option>
                            <option value={4}>STAND ALONE</option>
                            <option value={5}>GRANDES TIENDAS</option>
                            <option value={7}>COLOMBIA</option>
                        </Form.Field>

                        <Form.Message errorClass={`col-md-12 alert alert-danger`} for='cc.cc_unidad_negocio' />
                    </div>

                    <div className="clearfix"></div>

                    <div style={{ "marginTop": "10px" }} className="form-group">
                        <label>Tipo de Negocio</label>
                        <Form.Field className={`form-control col-md-12`} name='cc.cc_tipo_negocio' type='select'>
                            <option value={null}>Seleccionar área o unidad de negocio...</option>
                            <option value={1}>VENTA DISEÑO</option>
                            <option value={2}>VENTA SERVICIO GLOBAL</option>
                            <option value={3}>VENTA INTERCOMPAÑIA</option>
                            <option value={4}>SOLO  SUPERVISIÓN</option>
                            <option value={5}>SERVICIO POST VENTA</option>
                            <option value={6}>VENTA A SUMA ALZADA</option>
                            <option value={7}>ADMINISTRACION</option>
                        </Form.Field>


                        <Form.Message errorClass={`col-md-12 alert alert-danger`} for='cc.cc_tipo_negocio' />
                    </div>

                    <div className="clearfix"></div>

                    <div style={{ "marginTop": "10px" }} className="form-group">
                        <label>Jefe Proyecto</label>

                        <div>
                            <AsyncTypeahead
                                {...this.state}
                                labelKey="nombre_usuario"
                                minLength={3}
                                onSearch={this._handleSearch}
                                onChange={(e) => {
                                    this._jefeProyecto(e)
                                }}
                                placeholder="Buscar el jefe de proyecto ..."
                                renderMenuItemChildren={(option, props) => (
                                    <UserMenuItem key={option.id} user={option} />
                                    
                                )}
                            />

                        </div>                        

                        
                        <Form.Message errorClass={`col-md-12 alert alert-danger`} for='cc.cc_jefe_proyecto' />

                    </div>


                    <div className="clearfix"></div>

                    <label>Fecha estimativa de planimetria a cotizar </label>
                    <Form.Field name='cc.cc_fecha_planimetria' />
                    <Form.Message for='cc.cc_fecha_planimetria' />

                    <br/>

                    <label>Fecha estimativa de envio presupuesto a cliente </label>
                    <Form.Field name='cc.cc_fecha_presupuesto' />
                    <Form.Message for='cc.cc_fecha_presupuesto' />

                    <br />

                    <label>Fecha estimativa de inicio proyecto </label>
                    <Form.Field name='cc.cc_fecha_inicio' />
                    <Form.Message for='cc.cc_fecha_inicio' />
                    <br />

                    <label>Fecha estimativa de termino proyecto </label>
                    <Form.Field name='cc.cc_fecha_termino' />
                    <Form.Message for='cc.cc_fecha_termino' />


                    <div className="clearfix"></div>

                    <Form.Button style={{ "marginTop": "10px" }} className={`btn btn-block btn-success`} type='submit'>Enviar</Form.Button>
                </Form>
            </div>
            
        )
    }

}


export default AgregarCentroCosto;
