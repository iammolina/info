import Form from 'react-formal';
import React, { Component } from 'react';
import fecha_moment from 'moment/moment';
import UserMenuItem from "../Components/UserMenuItem";
import EmpresaMenuItem from "../Components/EmpresaMenuItem";
import { Alert } from 'react-bootstrap'
import { PutCc, findUsers, getEmpresas} from "../Api/centrocosto";
import { model_CentroCosto } from '../Schemas/CrearCentroCosto'
import { AsyncTypeahead } from 'react-bootstrap-typeahead'

export default class UpdateCentroCosto extends Component {

    constructor(props) {
        super();
        
        this.state = {
            model: model_CentroCosto,
            updated: false,
            jefe_proyecto: 0,
            jefe_area: 0,
            options: [],
            id: 0,
            isLoading : false,
            error: false
        }

        this.onSubmit = this.onSubmit.bind(this);
        this._handleSearch = this._handleSearch.bind(this);
    }

    onSubmit(e) {

        var c = {}
        var d = {}
        
        var a =
            {
               
                cc_fecha_inicio: fecha_moment(e.cc.cc_fecha_inicio).format('X'),
                cc_fecha_termino: fecha_moment(e.cc.cc_fecha_termino).format('X'),
                cc_fecha_presupuesto: fecha_moment(e.cc.cc_fecha_presupuesto).format('X'),
                cc_fecha_planimetria: fecha_moment(e.cc.cc_fecha_planimetria).format('X'),
                create_at: (Date.now() / 1000)
            }

       

        if (this.state.jefe_proyecto !== 0)
        {

             c = { cc_jefe_proyecto: this.state.jefe_proyecto };
        }


        if (this.state.jefe_area !== 0)
        {
             d = { cc_jefe_area: this.state.jefe_area };

        }
            

        var b = { ...e.cc, ...a , ...c , ...d };

       
        PutCc(e.cc.id_proyecto , b)
            .then((response) =>
            {
                this.setState({ updated : true });
                setTimeout(() => 
                {
                    this.setState({ updated: false });
                }, 3000);

            }).catch((error) => {
                alert('error');
            });

    }


    _jefeProyecto(e) {

        if (e[0]) {
            this.setState({ jefe_proyecto: e[0].id_usuario });

        } else {
            this.setState({ jefe_proyecto: '' });
        }


    }


    _jefeArea(e) {
        if (e[0]) {
            this.setState({ jefe_area: e[0].id_usuario });


        } else {
            this.setState({ jefe_area: '' });
        }

    }

    _handleSearch = (query) => {
        this.setState({ isLoading: true });
        var datos = { data: query };
        findUsers(datos) 
            .then((response) => {
                this.setState({
                    isLoading: false,
                    options: response.data.items
                });

            })
    }

    _handleSearchEmpresa = (query) => {
        this.setState({ isLoading: true });

        var datos = { data: query };

        getEmpresas(datos)
            .then((response) => {
                this.setState({
                    isLoading: false,
                    options: response.data
                });

            })
    }


    _emp(e) {
        if (e[0]) {
            this.setState({ _empresa: e[0].id_harvest });

        } else {
            this.setState({ _empresa: '' });
        }
    }



    render() {

      
        const {  updated } = this.state;

        const {cc} = this.props;


        if (!cc) return null;

        return (
            <div>

                {updated === true &&
                  

                    <Alert bsStyle="success" >
                         El centro costo ha sido actualizado
                    </Alert>
                  
                }

                <Form
                    onSubmit={(e) => this.onSubmit(e)}
                    onChange={modelInfo => this.setState({ model: modelInfo })}
                    defaultValue={{
                        cc
                    }}
                    schema={model_CentroCosto}
                >

                    <div className="row">
                        <div className="col-md-6">

                            <div style={{ "marginTop": "10px" }} className="form-group">
                                <label>Nombre Proyecto</label>
                                <Form.Field className={`form-control col-md-12`} name='cc.cc_nombre_proyecto' placeholder='Descripcion' />
                                <Form.Message errorClass={`col-md-12 alert alert-danger`} for='cc.cc_nombre_proyecto' />
                            </div>

                        </div>

                        <div className="col-md-6">
                            <div style={{ "marginTop": "10px" }} className="form-group">
                                <label>Ubicacion</label>
                                <Form.Field className={`form-control col-md-12`} name='cc.cc_ubicacion' placeholder='Descripcion' />
                                <Form.Message errorClass={`col-md-12 alert alert-danger`} for='cc.cc_ubicacion' />
                            </div>

                        </div>

                    </div>

                    <div className="clearfix"></div>


                    <div style={{ "marginTop": "10px" }} className="form-group">
                        <label>Empresa / Cliente</label>

                        <div>
                            <AsyncTypeahead
                                {...this.state}
                                labelKey="name"
                                minLength={3}
                                disabled
                                selected={[cc.nombre_empresa]}
                                onSearch={this._handleSearchEmpresa}
                                onChange={(e) => {
                                    this._emp(e)
                                }}
                                placeholder="Buscar la empresa ..."
                                renderMenuItemChildren={(option, props) => (
                                    <EmpresaMenuItem key={option.id} emp={option} />

                                )}
                            />

                        </div>

                        <Form.Message errorClass={`col-md-12 alert alert-danger`} for='cc.cc_jefe_area' />

                    </div>

                    <div className="clearfix"></div>

                    <div style={{ "marginTop": "10px" }} className="form-group">
                        <label>Project Manager / Cliente </label>
                        <Form.Field className={`form-control col-md-12`} name='cc.cc_cliente' placeholder='Descripcion' />
                        <Form.Message errorClass={`col-md-12 alert alert-danger`} for='cc.cc_cliente' />
                    </div>

                    <div className="clearfix"></div>

                    <div style={{ "marginTop": "10px" }} className="form-group">
                        <label>Direccion</label>
                        <Form.Field className={`form-control col-md-12`} name='cc.cc_direccion' placeholder='Descripcion' />
                        <Form.Message errorClass={`col-md-12 alert alert-danger`} for='cc.cc_direccion' />
                    </div>

                    <div className="clearfix"></div>

                    <div style={{ "marginTop": "10px" }} className="form-group">
                        <label>Superficie [m2]</label>
                        <Form.Field className={`form-control col-md-12`} name='cc.cc_superficie' placeholder='Descripcion' />
                    </div>

                    <div className="clearfix"></div>

                    <div style={{ "marginTop": "10px" }} className="form-group">
                        <label>Modelo de Negocio</label>
                        <Form.Field className={`form-control col-md-12`} name='cc.cc_unidad_negocio' type='select'>
                            <option value={null}>Seleccionar área o unidad de negocio...</option>
                            <option value={1}>ADMINISTRACIÓN INSTORE</option>
                            <option value={2}>CORNER</option>
                            <option value={3}>CONSTRUCTURA</option>
                            <option value={4}>STAND ALONE</option>
                            <option value={5}>GRANDES TIENDAS</option>
                        </Form.Field>

                        <Form.Message errorClass={`col-md-12 alert alert-danger`} for='cc.cc_unidad_negocio' />
                    </div>

                    <div className="clearfix"></div>

                    <div style={{ "marginTop": "10px" }} className="form-group">
                        <label>Tipo de Negocio</label>
                        <Form.Field className={`form-control col-md-12`} name='cc.cc_tipo_negocio' type='select'>
                            <option value={null}>Seleccionar área o unidad de negocio...</option>
                            <option value={1}>VENTA DISEÑO</option>
                            <option value={2}>VENTA SERVICIO GLOBAL</option>
                            <option value={3}>VENTA INTERCOMPAÑIA</option>
                            <option value={4}>SOLO SUPERVISIÓN</option>
                            <option value={5}>SERVICIO POST VENTA</option>
                            <option value={6}>VENTA A SUMA ALZADA</option>
                            <option value={7}>ADMINISTRACION</option>
                        </Form.Field>

                        <Form.Message errorClass={`col-md-12 alert alert-danger`} for='cc.cc_tipo_negocio' />
                    </div>

                    <div className="clearfix"></div>

                    <div style={{ "marginTop": "10px" }} className="form-group">
                        <label>Jefe Proyecto</label>

                        <div>
                            <AsyncTypeahead
                                {...this.state}
                                labelKey="nombre_usuario"
                                minLength={3}
                                selected={[cc.jefe_proyecto]}
                                onSearch={this._handleSearch}
                                onChange={(e) => {
                                    this._jefeProyecto(e)
                                }}
                                placeholder="Buscar el jefe de proyecto ..."
                                renderMenuItemChildren={(option, props) => (
                                    <UserMenuItem key={option.id} user={option} />

                                )}
                            />

                        </div>

                        <Form.Message errorClass={`col-md-12 alert alert-danger`} for='cc.cc_jefe_proyecto' />
                    </div>

                    <div className="clearfix"></div>

                    <div style={{ "marginTop": "10px" }} className="form-group">
                        <label>Jefe Área</label>

                        <div>
                            <AsyncTypeahead
                                {...this.state}
                                labelKey="nombre_usuario"
                                minLength={3}
                                selected={[cc.jefe_area]}
                                onSearch={this._handleSearch}
                                onChange={(e) => {
                                    this._jefeArea(e)
                                }}
                                placeholder="Buscar el jefe de area ..."
                                renderMenuItemChildren={(option, props) => (
                                    <UserMenuItem key={option.id} user={option} />

                                )}
                            />
                        </div>

                        <Form.Message errorClass={`col-md-12 alert alert-danger`} for='cc.cc_jefe_area' />
                       
                    </div>

                    <div className="clearfix"></div>

                    <label>Fecha estimativa de planimetria a cotizar </label>
                    <Form.Field name='cc.cc_fecha_planimetria' />
                    <Form.Message for='cc.cc_fecha_planimetria' />

                    <br />

                    <label>Fecha estimativa de envio presupuesto a cliente </label>
                    <Form.Field name='cc.cc_fecha_presupuesto' />
                    <Form.Message for='cc.cc_fecha_presupuesto' />

                    <br />

                    <label>Fecha estimativa de inicio proyecto </label>
                    <Form.Field name='cc.cc_fecha_inicio' />
                    <Form.Message for='cc.cc_fecha_inicio' />
                    <br />

                    <label>Fecha estimativa de termino proyecto </label>
                    <Form.Field name='cc.cc_fecha_termino' />
                    <Form.Message for='cc.cc_fecha_termino' />


                    <div className="clearfix"></div>

                    <Form.Button style={{ "marginTop": "10px" }} className={`btn btn-block btn-success`} type='submit'>Actualizar Centro Costo</Form.Button>

                </Form>
            </div>
        )


    }
}