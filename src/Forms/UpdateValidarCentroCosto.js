
import Form from 'react-formal';
import React, { Component } from 'react';
import { Alert } from 'react-bootstrap';
import { PutCc } from "../Api/centrocosto";
import { model_CentroCosto } from '../Schemas/CrearCentroCosto'


export default class UpdateValidarCentroCosto extends Component {

    constructor(props) {
        super();
        
        this.state = {
            model: model_CentroCosto,
            updated: false,
            id: 0,
            error: false
        }

        this.onSubmit = this.onSubmit.bind(this);
    }

    onSubmit(e) {

        PutCc(e.cc.id_proyecto , e.cc)
            .then((response) => {

                this.setState({ updated : true });

            }).catch((error) => {
                alert('error');
            });

    }



    render() {

      
        const {  updated } = this.state;

        const { cc } = this.props;


        if (!cc) return null;

        return (
            <div>
                {updated === true &&

                    <Alert bsStyle="success" >
                         <strong>El centro costo ha sido actualizado</strong>
                    </Alert>
                  
                }

                <Form
                    onSubmit={(e) => this.onSubmit(e)}
                    onChange={modelInfo => this.setState({ model: modelInfo })}
                    defaultValue={{
                        cc
                    }}
                    schema={model_CentroCosto}
                >

                    <div class="row">
                        <div class="col-md-6">

                            <div style={{ "marginTop": "10px" }} className="form-group">
                                <label>Nombre Proyecto</label>
                                <Form.Field className={`form-control col-md-12`} name='cc.cc_nombre_proyecto' placeholder='Descripcion' />
                                <Form.Message errorClass={`col-md-12 alert alert-danger`} for='cc.cc_nombre_proyecto' />
                            </div>

                        </div>

                        <div class="col-md-6">


                            <div style={{ "marginTop": "10px" }} className="form-group">
                                <label>Ubicacion</label>
                                <Form.Field className={`form-control col-md-12`} name='cc.cc_ubicacion' placeholder='Descripcion' />
                                <Form.Message errorClass={`col-md-12 alert alert-danger`} for='cc.cc_ubicacion' />
                            </div>

                        </div>

                    </div>

                   
                    <div className="clearfix"></div>

                    <div style={{ "marginTop": "10px" }} className="form-group">
                        <label>Area o Unidad de negocio</label>
                        <Form.Field className={`form-control col-md-12`} name='cc.cc_unidad_negocio' type='select'>
                            <option value={null}>Seleccionar área o unidad de negocio...</option>
                            <option value={1}>ADMINISTRACIÓN INSTORE</option>
                            <option value={2}>CORNER</option>
                            <option value={3}>CONSTRUCTURA</option>
                            <option value={4}>STAND ALONE</option>
                            <option value={5}>GRANDES TIENDAS</option>
                        </Form.Field>

                        <Form.Message errorClass={`col-md-12 alert alert-danger`} for='cc.cc_unidad_negocio' />
                    </div>

                    <div className="clearfix"></div>

                    <div style={{ "marginTop": "10px" }} className="form-group">
                        <label>Tipo de Negocio</label>
                        <Form.Field className={`form-control col-md-12`} name='cc.cc_tipo_negocio' type='select'>
                            <option value={null}>Seleccionar área o unidad de negocio...</option>
                            <option value={1}>VENTA DISEÑO</option>
                            <option value={2}>VENTA SERVICIO GLOBAL</option>
                            <option value={3}>VENTA INTERCOMPAÑIA</option>
                            <option value={4}>SOLO  SUPERVISIÓN</option>
                            <option value={5}>SERVICIO POST VENTA</option>
                            <option value={6}>VENTA A SUMA ALZADA</option>
                            <option value={7}>ADMINISTRACION</option>
                        </Form.Field>

                        <Form.Message errorClass={`col-md-12 alert alert-danger`} for='cc.cc_tipo_negocio' />
                    </div>
                    

                    <div className="clearfix"></div>

                    <Form.Button style={{ "marginTop": "10px" }} className={`btn btn-block btn-success`} type='submit'>Actualizar Centro Costo</Form.Button>

                </Form>
            </div>
        )


    }
}