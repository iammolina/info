import Form from 'react-formal';
import React, { Component } from 'react';
import { Alert } from 'react-bootstrap'
import { model_facturar } from '../Schemas/Factura'
import { putFacturar } from "../Api/facturar";


export default class UpdateFactura extends Component 
{

    constructor(props) {
        super();

        this.state = {
            model: model_facturar,
            updated: false,
        }

        this.onSubmit = this.onSubmit.bind(this);
    }

    onSubmit(e) 
    {
        putFacturar(e)
            .then((response) => 
            {   

                this.setState({updated: true});
              

            }).catch((error) => {
                alert('error');
            });

    }
   


    render() {

        const { updated } = this.state;

        const { fact } = this.props;


        if (!fact) return null;

        return (
            <div>
                {updated === true &&

                    <Alert bsStyle="success" >
                        La solicitud se actualizo , correctamente.
                    </Alert>

                }

                <Form
                    onSubmit={(e) => this.onSubmit(e)}
                    onChange={modelInfo => this.setState({ model: modelInfo })}
                    defaultValue={{
                        fact
                    }}
                    schema={model_facturar}

                >
                        <div className="clearfix"></div>


                        <div style={{ "marginTop": "10px" }} className="form-group">
                            <label>Monto</label>
                            <Form.Field className={`form-control col-md-12`} name='fact.monto' placeholder='Descripcion' />
                            <Form.Message errorClass={`col-md-12 alert alert-danger`} for='fact.monto' />
                        </div>

                        <div className="clearfix"></div>

                        <div style={{ "marginTop": "10px" }} className="form-group">
                            <label>Numero OC</label>
                            <Form.Field className={`form-control col-md-12`} name='fact.oc' placeholder='Descripcion' />
                            <Form.Message errorClass={`col-md-12 alert alert-danger`} for='fact.oc' />
                        </div>

                        <div style={{ "marginTop": "10px" }} className="form-group">
                            <label>Detalle</label>
                            <Form.Field className={`form-control col-md-12`} name='fact.detalle' placeholder='Descripcion' />
                            <Form.Message errorClass={`col-md-12 alert alert-danger`} for='fact.detalle' />
                        </div>

                        <div className="clearfix"></div>

                        <Form.Button style={{ "marginTop": "50px" }} className={`btn btn-block btn-success`} type='submit'>Actualizar Solicitud</Form.Button>
                    
                        
                </Form>

               
            </div>
        )


    }
}