import Form from 'react-formal';
import jwtDecode from 'jwt-decode';
import React, { Component } from 'react';
import { Alert } from 'react-bootstrap'
import { model_facturar } from '../Schemas/Factura'
import { putFacturarAceptar } from "../Api/facturar";


export default class AceptarFactura extends Component 
{
    constructor(props) {
        super();

        this.state = {
            model: model_facturar,
            updated: false,
            user: JSON.parse(localStorage.getItem('user'))
        }

        this.onSubmit = this.onSubmit.bind(this);
    }

    onSubmit(e) 
    {
        const token = jwtDecode(this.state.user);
        const u = token[0];

        const obj =
        {
            id : e.fact.id,
            num_factura: e.fact.num_factura,
            estado : 2,
            id_realizado : u.id_usuario,
            fecha_realizado : new Date(),
        }
        putFacturarAceptar(obj)
            .then((response) => 
            {   
                this.setState({updated: true});

            }).catch((error) => {
                alert('error');
            });

    }
   

    render() {

        const { updated } = this.state;

        const { fact } = this.props;


        if (!fact) return null;

        return (
            <div>
                {updated === true &&

                    <Alert bsStyle="success" >
                        La solicitud se actualizo , correctamente.
                    </Alert>

                }

                <Form
                    onSubmit={(e) => this.onSubmit(e)}
                    onChange={modelInfo => this.setState({ model: modelInfo })}
                    defaultValue={{
                        fact
                    }}
                    schema={model_facturar}
                >
                        <div className="clearfix"></div>

                        <div style={{ "marginTop": "10px" }} className="form-group">
                            <label>Numero Factura</label>
                            <Form.Field className={`form-control col-md-12`} name='fact.num_factura' placeholder='Descripcion' />
                            <Form.Message errorClass={`col-md-12 alert alert-danger`} for='fact.num_factura' />
                        </div>

                        
                        <div className="clearfix"></div>

                        <Form.Button style={{ "marginTop": "10px" }} className={`btn btn-block btn-success`} type='submit'>Aceptar Solicitud</Form.Button>

                </Form>
            </div>
        )


    }
}